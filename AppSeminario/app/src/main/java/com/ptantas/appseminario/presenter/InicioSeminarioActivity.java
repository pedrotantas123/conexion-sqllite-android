package com.ptantas.appseminario.presenter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.ptantas.appseminario.R;

import java.util.Date;

public class InicioSeminarioActivity extends AppCompatActivity {
    public static final String TAG="InicioSeminarioActivity";

    private int IdAsis;
    private String nombreAsis;
    private String apellidoAsis;
    private Date fecNacAsis;
    private String dniAsis;
    private String sexoAsis;
    private String emailAsis;
    private String telefonoAsis;

    EditText edtIdAsis, edtnombreAsis, edtapellidoAsis, edtfecNacAsis, edtdniAsis, edtsexoAsis, edtemailAsis, edttelefonoAsis;
    Button btnNuevo, btnActualizar, btnEliminar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio_seminario);
    }

    public void initObjects(){

        edtIdAsis = findViewById(R.id.edtIdAsis);
        edtnombreAsis = findViewById(R.id.edtnombreAsis);
        edtapellidoAsis = findViewById(R.id.edtapellidoAsis);
        edtfecNacAsis = findViewById(R.id.edtfecNacAsis);
        edtdniAsis = findViewById(R.id.edtdniAsis);
        edtsexoAsis = findViewById(R.id.edtsexoAsis);
        edtemailAsis = findViewById(R.id.edtemailAsis);
        edttelefonoAsis = findViewById(R.id.edttelefonoAsis);
        btnNuevo = findViewById(R.id.btnNuevo);
        btnActualizar = findViewById(R.id.btnActualizar);
        btnEliminar = findViewById(R.id.btnEliminar);
    }


    public void onClickNuevo(View view){
        Log.d(TAG, ">>>>Ingreso a metodo onClickNuevo()");
    }

    public void onClickActualizar(View view){
        Log.d(TAG, ">>>>Ingreso a metodo onClickActualizar()");
    }

    public void onClickEliminar(View view){
        Log.d(TAG, ">>>>Ingreso a metodo onClickEliminar()");
    }

}
