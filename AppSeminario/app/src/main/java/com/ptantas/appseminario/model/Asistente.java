package com.ptantas.appseminario.model;

import java.util.Date;

public class Asistente {
    private int IdAsis;
    private String nombreAsis;
    private String apellidoAsis;
    private Date fecNacAsis;
    private String dniAsis;
    private String sexoAsis;
    private String emailAsis;
    private String telefonoAsis;

    //Constructor


    public Asistente() {}

    public Asistente(int IdAsis, String nombreAsis, String apellidoAsis, Date fecNacAsis, String dniAsis, String sexoAsis, String emailAsis, String telefonoAsis) {
        this.IdAsis = IdAsis;
        this.nombreAsis = nombreAsis;
        this.apellidoAsis = apellidoAsis;
        this.fecNacAsis = fecNacAsis;
        this.dniAsis = dniAsis;
        this.sexoAsis = sexoAsis;
        this.emailAsis = emailAsis;
        this.telefonoAsis = telefonoAsis;
    }

    //Getter and Setter

    public int getIdAsis() {
        return IdAsis;
    }

    public void setIdAsis(int idAsis) {
        this.IdAsis = idAsis;
    }

    public String getNombreAsis() {
        return nombreAsis;
    }

    public void setNombreAsis(String nombreAsis) {
        this.nombreAsis = nombreAsis;
    }

    public String getApellidoAsis() {
        return apellidoAsis;
    }

    public void setApellidoAsis(String apellidoAsis) {
        this.apellidoAsis = apellidoAsis;
    }

    public Date getFecNacAsis() {
        return fecNacAsis;
    }

    public void setFecNacAsis(Date fecNacAsis) {
        this.fecNacAsis = fecNacAsis;
    }

    public String getDniAsis() {
        return dniAsis;
    }

    public void setDniAsis(String dniAsis) {
        this.dniAsis = dniAsis;
    }

    public String getSexoAsis() {
        return sexoAsis;
    }

    public void setSexoAsis(String sexoAsis) {
        this.sexoAsis = sexoAsis;
    }

    public String getEmailAsis() {
        return emailAsis;
    }

    public void setEmailAsis(String emailAsis) {
        this.emailAsis = emailAsis;
    }

    public String getTelefonoAsis() {
        return telefonoAsis;
    }

    public void setTelefonoAsis(String telefonoAsis) {
        this.telefonoAsis = telefonoAsis;
    }
}
